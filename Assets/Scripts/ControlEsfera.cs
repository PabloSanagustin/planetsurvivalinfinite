﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;


public class ControlEsfera : MonoBehaviour
{
    Rigidbody rb;
    public float velocidad;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        float movH = CrossPlatformInputManager.GetAxis("Horizontal") * velocidad;
        float movV = CrossPlatformInputManager.GetAxis("Vertical") * velocidad;

        rb.AddForce(movH, 0, movV);

    }
}
