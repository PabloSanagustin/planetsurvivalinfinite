﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyObjects : MonoBehaviour
{
    public GameObject Explosion;
   // public FinalExplosionObjects finalExplosionObjects;
   /*
    public void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.CompareTag("PlayerShot"))
        {
            // Destroy(this.gameObject,3);
            Invoke("DestroyObject", 3);
            GameObject clone = (GameObject)Instantiate(Explosion, transform.position, transform.rotation);
            Destroy(clone, 3);
           // finalExplosionObjects.FinalExplosionObjectsDestroy();
            Debug.Log("It Collides");

        }
    }
    */

    public void OnTriggerEnter(Collider other)
    {
        if(other.tag == "PlayerShot")
        {
            // Destroy(this.gameObject,3);
            Invoke("DestroyObject", 3);
            GameObject clone = (GameObject)Instantiate(Explosion, transform.position, transform.rotation);
            Destroy(clone, 3);
            // finalExplosionObjects.FinalExplosionObjectsDestroy();
            //Debug.Log("It Collides");
        }
        if(other.tag == "Enemy") ///////
        {
            this.gameObject.SetActive(false);
        }
    }
    void DestroyObject()
    {
        this.gameObject.SetActive(false);
    }
}
