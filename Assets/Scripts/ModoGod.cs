﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModoGod : MonoBehaviour
{
    //private Rigidbody rigidBodyPlayer;
    //private BoxCollider
    public float velocityTranslate = 5;

    private void Start()
    {
        //rigidBodyPlayer.GetComponent<Rigidbody>();


    }
    // Update is called once per frame
    void Update()
    {
        if(Input.GetKey(KeyCode.U))
        {
            transform.Translate(Vector3.up * velocityTranslate * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.J))
        {
            transform.Translate(Vector3.down * velocityTranslate * Time.deltaTime);

        }
        if(Input.GetKey(KeyCode.H))
        {
            transform.Translate(Vector3.left * velocityTranslate * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.K))
        {
            transform.Translate(Vector3.right * velocityTranslate * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.N))
        {
            transform.Translate(Vector3.forward * velocityTranslate * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.B))
        {
            transform.Translate(Vector3.back * velocityTranslate * Time.deltaTime);
        }


    }
}
