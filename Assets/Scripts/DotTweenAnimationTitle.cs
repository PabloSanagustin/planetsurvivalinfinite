﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class DotTweenAnimationTitle : MonoBehaviour
{
    public GameObject Title;
    public GameObject AllExceptTitle;
    public RectTransform title, allExceptTitle;

    // Start is called before the first frame update
    void Start()
    {
        Time.timeScale = 1;
        Sequence mySequence = DOTween.Sequence();

       // mySequence.Append( Title.transform.DOMove(new Vector3(Title.transform.position.x, 300, Title.transform.position.y), 1));
       // mySequence.Append( AllExceptTitle.transform.DOMove(new Vector3(AllExceptTitle.transform.position.x, 225, AllExceptTitle.transform.position.y), 1));
       mySequence.Append( title.DOAnchorPos(Vector2.zero, 1f));
       mySequence.Append( allExceptTitle.DOAnchorPos(Vector2.zero, 1f));
       
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
