﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyOtherPlayer : MonoBehaviour
{

    public GameObject Explosion;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            /*
            Destroy(other.gameObject);
            GameObject clone = (GameObject)Instantiate(Explosion, other.gameObject.transform.position, other.gameObject.transform.rotation);
            Destroy(clone, 3); */

            ScoreManager.score += Random.Range(1, 50);
            other.GetComponent<PlayerLifeOnline>().DamagePlayerOnline();
            GameObject clone = (GameObject)Instantiate(Explosion, other.gameObject.transform.position, other.gameObject.transform.rotation);
            Destroy(clone, 1);
        }


    }
}
