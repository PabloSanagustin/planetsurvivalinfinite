﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class MixLevels : MonoBehaviour
{
    // Start is called before the first frame updatexer
    public AudioMixer masterMixer;
    public void SetFXVolume(float fxVolume)
    {
        masterMixer.SetFloat("Sounds", fxVolume);
    }
    public void SetMusicVolume(float musicVolume)
    {
        masterMixer.SetFloat("Music", musicVolume);
    }
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
