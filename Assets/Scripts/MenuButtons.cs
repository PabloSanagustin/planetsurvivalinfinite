﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuButtons : MonoBehaviour
{
    public Text scoreText;
    public Text recordText;

    public void Start()
    {
        int score = 0;
        if (PlayerPrefs.HasKey("MyScore"))
        {
            score = PlayerPrefs.GetInt("MyScore");
        }
        scoreText.text = "Score:" + score.ToString("00000");


        int record = 0;
        if (PlayerPrefs.HasKey("MyRecord"))
        {
            record = PlayerPrefs.GetInt("MyRecord");
        }
        recordText.text = "Record:" + record.ToString("00000");
    }

    public void Exit()
    {
        Application.Quit();
    }
    public void PlayAgain()
    {
         SceneManager.LoadScene("GameplayProcedural");
        //Application.LoadLevel(0);
    }
    public void GoToMenu()
    {
        SceneManager.LoadScene("Menu");
    }

    public void Options()
    {
        SceneManager.LoadScene("Options");
    }
}
