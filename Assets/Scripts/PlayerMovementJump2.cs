﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
//using UnityEngine.Experimental.UIElements;
using UnityEngine.UI;

public class PlayerMovementJump2 : MonoBehaviour
{
    public AudioSource Corriendo;
    public AudioSource Jump;
    public Toggle m_Toggle;

    public GameObject GenerateSeed;


    public CharacterController controller;

    private ModoGod modoGod;

    private BoxCollider boxColliderPlayer;

    public bool rotate;
    public bool rotateUpDown;
    public bool rLeft;
    public bool uDown;
    public bool jumpBool;

    public bool AndandoJoystick;

    public float speed = 6.0F;
    public float jumpSpeed = 600.0F;
    public float gravity = 20.0F;
    public float rotateSpeed = 8.0f;
    private Vector3 moveDirection = Vector3.zero;
    private Vector3 moveDirection2 = Vector3.zero;


    public bool IsGrounded;
    private Rigidbody rigidbodyCharacter;


    //private Vector3 inputVector;
    // Start is called before the first frame update


    // Update is called once per frame

    public float speedPCPlayerAvanzar;
    private Animator animatorCharacter;

    private void Start()
    {
        boxColliderPlayer = GetComponent<BoxCollider>();

        modoGod = GetComponent<ModoGod>(); //


        rigidbodyCharacter = GetComponent<Rigidbody>();
        animatorCharacter = GetComponentInChildren<Animator>();
    }
    private void FixedUpdate()
    {
       // rigidbodyCharacter.velocity = inputVector;
        float moveHorizontal = Input.GetAxis("Horizontal2");
        float moveVertical = Input.GetAxis("Vertical2");
        Vector3 movement = new Vector3(/*moveHorizontal*/0.0f, 0.0f, moveVertical);

        //rigidbodyCharacter.AddForce(movement * speed);

        //rigidbodyCharacter.transform.Translate(movement * Time.fixedDeltaTime);
        


    }
    void Update()
    {
        /*
        if(Input.GetAxis("Vertical") <= -1)
        {
            GenerateSeed.transform.localPosition = new Vector3(GenerateSeed.transform.localPosition.x, GenerateSeed.transform.localPosition.y, GenerateSeed.transform.localPosition.z * -1);
        }

        if (Input.GetAxis("Vertical") > -1)
        {
            GenerateSeed.transform.localPosition = new Vector3(GenerateSeed.transform.localPosition.x, GenerateSeed.transform.localPosition.y, GenerateSeed.transform.localPosition.z * +1);
        }

        if (Input.GetAxis("Vertical2") <= -1)
        {
            GenerateSeed.transform.localPosition = new Vector3(GenerateSeed.transform.localPosition.x, GenerateSeed.transform.localPosition.y, GenerateSeed.transform.localPosition.z * -1);
        }

        if (Input.GetAxis("Vertical2") > -1)
        {
            GenerateSeed.transform.localPosition = new Vector3(GenerateSeed.transform.localPosition.x, GenerateSeed.transform.localPosition.y, GenerateSeed.transform.localPosition.z * +1);
        }
        */ ////////


        /*
        if (Input.GetAxis("Horizontal") <= -1)
        {
            GenerateSeed.transform.localPosition = new Vector3(GenerateSeed.transform.localPosition.x, GenerateSeed.transform.localPosition.y, GenerateSeed.transform.localPosition.z * -1);
        }

        if (Input.GetAxis("Horizontal") > -1)
        {
            GenerateSeed.transform.localPosition = new Vector3(GenerateSeed.transform.localPosition.x, GenerateSeed.transform.localPosition.y, GenerateSeed.transform.localPosition.z * +1);
        }

        if (Input.GetAxis("Horizontal2") <= -1)
        {
            GenerateSeed.transform.localPosition = new Vector3(GenerateSeed.transform.localPosition.x, GenerateSeed.transform.localPosition.y, GenerateSeed.transform.localPosition.z * -1);
        }

        if (Input.GetAxis("Horizontal2") > -1)
        {
            GenerateSeed.transform.localPosition = new Vector3(GenerateSeed.transform.localPosition.x, GenerateSeed.transform.localPosition.y, GenerateSeed.transform.localPosition.z * +1);
        }
        */
        ModoGod();

        if (controller.isGrounded)
        {
            moveDirection = new Vector3(0/*Input.GetAxis("Horizontal")*/, 0, Input.GetAxis("Vertical2"));
            moveDirection = transform.TransformDirection(moveDirection);
            moveDirection *= speed;
            if (Input.GetButton("Jump"))
            {
                moveDirection.y = jumpSpeed;
            }
            if (jumpBool)
            {
                moveDirection.y = jumpSpeed;
            }
        }


        if(IsGrounded == true)
        {
            animatorCharacter.SetTrigger("Idle");
            animatorCharacter.ResetTrigger("Jump");
            moveDirection = new Vector3(0/*Input.GetAxis("Horizontal")*/, 0, Input.GetAxis("Vertical2"));
            moveDirection = transform.TransformDirection(moveDirection);
            moveDirection *= speed;
            if (Input.GetButton("Jump"))
            {
                Jump.Play();
                // moveDirection.y = jumpSpeed;
                rigidbodyCharacter.AddForce(new Vector3(0, jumpSpeed, 0), ForceMode.Impulse);
                //animatorCharacter.SetTrigger("Jump");
                //animatorCharacter.ResetTrigger("Walk");

            }
            if (jumpBool)
            {
                Jump.Play();
                // moveDirection.y = jumpSpeed;
                rigidbodyCharacter.AddForce(new Vector3(0, jumpSpeed, 0), ForceMode.Impulse);
               // animatorCharacter.SetTrigger("Jump");
               // animatorCharacter.ResetTrigger("Walk");

            }
        }
        if(IsGrounded == false)
        {
            animatorCharacter.SetTrigger("Jump");
            animatorCharacter.ResetTrigger("Idle");
            //animatorCharacter.ResetTrigger("Idle");
            animatorCharacter.ResetTrigger("Walk");


        }

        moveDirection.y -= gravity * Time.deltaTime;
        

       // controller.Move(moveDirection * Time.deltaTime);//


        //rigidbodyCharacter.AddForce(moveDirection, ForceMode.Impulse); //
        
       if(Input.GetAxis("Vertical2") != 0)
       {
             ///////////////////////////////////////////////////////////////
            /*rigidbodyCharacter. */transform.Translate(Vector3.forward * Input.GetAxis("Vertical2")* speed * Time.deltaTime);
            //(transform.position.x, transform.position.y, speed * Time.fixedDeltaTime)
            if(IsGrounded == true)
            {
               // Corriendo.Play();

                animatorCharacter.SetTrigger("Walk");
                animatorCharacter.ResetTrigger("Idle");
                //animatorCharacter.ResetTrigger("Idle");
                animatorCharacter.ResetTrigger("Jump");
            }
            
            //animatorCharacter.ReseTrigger("Walk");
        }

        
         if(Input.anyKey == false)
          {
             // animatorCharacter.SetTrigger("Idle");
          } 
        /*
        if(Input.GetAxis("Vertical2") == 0)
         {
             animatorCharacter.SetTrigger("Idle");
         } */



        //Rotate Player
        transform.Rotate(0, Input.GetAxis("Horizontal2") * rotateSpeed * Time.deltaTime, 0); //////

        //inputVector = new Vector3(rigidbodyCharacter.velocity.x, rigidbodyCharacter.velocity.y, Input.GetAxisRaw("Vertical") * speed);
        //transform.LookAt(transform.position + new Vector3(inputVector.x, 0, inputVector.z));
        

        /////////////////////////
        ///


        if (rotate)
        {
            
            if (rLeft)
            {
               // Corriendo.Play();

                gameObject.transform.Translate(Vector3.back * speed * Time.deltaTime);
                if (IsGrounded == true)
                {
                    //Corriendo.Play(); /////////////
                    animatorCharacter.SetTrigger("Walk");
                    animatorCharacter.ResetTrigger("Idle");
                    //animatorCharacter.ResetTrigger("Idle");
                    animatorCharacter.ResetTrigger("Jump");
                }
            }
            else
            {
               // Corriendo.Play();


                gameObject.transform.Translate(Vector3.forward * speed * Time.deltaTime);

                if (IsGrounded == true)
                {
                    //Corriendo.Play();////////
                    animatorCharacter.SetTrigger("Walk");
                    animatorCharacter.ResetTrigger("Idle");
                    //animatorCharacter.ResetTrigger("Idle");
                    animatorCharacter.ResetTrigger("Jump");
                }
            }

        }
        if(rotateUpDown)
        {
            if(uDown)
            {
                gameObject.transform.Rotate(Vector3.up, rotateSpeed * Time.deltaTime);
            }
            else
            {
                gameObject.transform.Rotate(Vector3.down, rotateSpeed * Time.deltaTime);
            }
        }
        if(AndandoJoystick)
        {
            if (IsGrounded == true)
            {
                //Corriendo.Play();
                //Corriendo.Play(); ////////


                animatorCharacter.SetTrigger("Walk");
                animatorCharacter.ResetTrigger("Idle");
                //animatorCharacter.ResetTrigger("Idle");
                animatorCharacter.ResetTrigger("Jump");
            }
        }

        //fly Player
        //transform.Translate(0, Input.GetAxis("UpDown") * controller., 0);
        /*
        if(Input.GetButton("Fly"))
        {
            moveDirection2.y = jumpSpeed;
        }
        controller.Move(moveDirection2 * Time.deltaTime);
        */

    }

    public void jump()
    {
        //moveDirection.y = jumpSpeed;
        jumpBool = true;
    }
    public void Delante()
    {
        /*
        if (IsGrounded == false)
        {
            Corriendo.Play();
            //Corriendo.loop = m_Toggle.isOn;

        } */
        // moveDirection = new Vector3(0/*Input.GetAxis("Horizontal")*/, 0, speed);
        // Update();
        uDown = false;
        rotate = true;
        rLeft = false;
        rotateUpDown = false;
        //animatorCharacter.SetTrigger("Walk");
    }

    public void Andando()
    {/*
        if (IsGrounded == false)
        {
            Corriendo.Play();
            //Corriendo.loop = m_Toggle.isOn;

        } */
        AndandoJoystick = true;
    }
    public void Atras()
    {
        /*
        if (IsGrounded == false)
        {
            Corriendo.Play();
            //Corriendo.loop = m_Toggle.isOn;

        } */
        //moveDirection = new Vector3(0/*Input.GetAxis("Horizontal")*/, 0, -speed);
        // Update();
        uDown = false;
        rotate = true;
        rLeft = true;
        rotateUpDown = false;
        //animatorCharacter.SetTrigger("Walk");
    }
    public void Derecha()
    {
        /*
        if (IsGrounded == false)
        {
            Corriendo.Play();
            //Corriendo.loop = m_Toggle.isOn;

        } */
        //Rotate Player
        // Debug.Log("Derecha");
        // transform.Rotate(0, speed, 0);
        uDown = true;
        rotate = false;
        rLeft = false;
        rotateUpDown = true;
    }
    public void Izquierda()
    {
        uDown = false;
        rotate = false;
        rLeft = false;
        rotateUpDown = true;
        /*
        if ( IsGrounded == false)
        {
            Corriendo.Play();
            //Corriendo.loop = m_Toggle.isOn;

        }*/
        // Debug.Log("Izquierda");
        // transform.Rotate(0, -speed, 0);
    }

    public void RotateOff()
    {
        rotate = false;
        rotateUpDown = false;
        jumpBool = false;
        animatorCharacter.SetTrigger("Idle");
        AndandoJoystick = false; ///
    }

    private void OnCollisionStay(Collision collision)
    {
        IsGrounded = true;
        
    }
    private void OnCollisionExit(Collision collision)
    {
        IsGrounded = false;
    }
    private void OnCollisionEnter(Collision collision)
    {
        // if(IsGrounded == true)
        // {
        /*
        if (Input.GetAxis("Vertical2") != 0 && IsGrounded == true && rigidbodyCharacter.velocity.magnitude > 0.1f)
        {
            Corriendo.Play();
            //Corriendo.loop = m_Toggle.isOn;

        } */
        //}
    }


    public void ModoGod()
    {
        if(Input.GetKeyUp(KeyCode.P))
        {
            modoGod.enabled = true;
            rigidbodyCharacter.detectCollisions = false;
            rigidbodyCharacter.isKinematic = true;

            boxColliderPlayer.enabled = false;
        }

        if(Input.GetKeyUp(KeyCode.O))
        {
             modoGod.enabled = false;
            rigidbodyCharacter.detectCollisions = true;
            rigidbodyCharacter.isKinematic = false;
            boxColliderPlayer.enabled = true;
        }
        if(Input.GetKeyUp(KeyCode.M))
        {
            ScoreManager.score = 49;
        }


    }


}
