﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{
    public static int score;
    public static int record;
    Text text;

    private void Awake()
    {
        text = GetComponent<Text>();
        score = 0;
        if (PlayerPrefs.HasKey("MyRecord"))
        {
            record = PlayerPrefs.GetInt("MyRecord");
        }
    }

    // Update is called once per frame
    void Update()
    {
        text.text = "Score: " + score;
    }
}
