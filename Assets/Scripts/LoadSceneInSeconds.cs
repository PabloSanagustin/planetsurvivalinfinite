﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadSceneInSeconds : MonoBehaviour
{
    public float seconds = 5f;
    // Start is called before the first frame update
    void Start()
    {
        Invoke("CambiarDeEscenaEnSegundos", seconds);
    }

    // Update is called once per frame
    void CambiarDeEscenaEnSegundos()
    {
        SceneManager.LoadScene("Menu");
    }
}
