﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisableIfFarAway : MonoBehaviour
{
    // Start is called before the first frame update

    private GameObject itemActivatorObject;
    private ItemActivator activationScript;

    public float TiempoParaQueAparezca = 1.0f;
    public float EsperarAlPrincipio = 1.0f;

    void Start()
    {
        itemActivatorObject = GameObject.Find("GameManager");
        activationScript = itemActivatorObject.GetComponent<ItemActivator>();
        //StartCoroutine("AddToList"); ///
        InvokeRepeating("Spawn", EsperarAlPrincipio, TiempoParaQueAparezca);
        //StartCoroutine("AddToList"); ///

    }

    public void Spawn()
    {
        StartCoroutine("AddToList"); ///
    }
    /*
    private void FixedUpdate()
    {
        
    }
    private void Update()
    {
       // StartCoroutine("AddToList"); ///

    }
    */
    IEnumerator AddToList()
    {
        yield return new WaitForSeconds(0.1f);
        activationScript.activatorItems.Add(new ActivatorItem { item = this.gameObject, itemPos = transform.position });
    }


}
