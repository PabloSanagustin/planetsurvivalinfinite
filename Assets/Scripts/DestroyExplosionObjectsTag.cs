﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyExplosionObjectsTag : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "ExplosionEnemy")
        {
            Destroy(this.gameObject);
        }
    }
}
