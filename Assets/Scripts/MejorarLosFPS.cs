﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MejorarLosFPS : MonoBehaviour
{
    [SerializeField]
    float factor = 0.5f;
    // Start is called before the first frame update
    void Start()
    {
        QualitySettings.vSyncCount = 0;
        Application.targetFrameRate = -1;


        Screen.SetResolution(
                    Mathf.CeilToInt(Screen.currentResolution.width * factor),
                    Mathf.CeilToInt(Screen.currentResolution.height * factor),
                    true
            );
        
    }


}
