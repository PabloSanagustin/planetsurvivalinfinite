﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShotOnline : Photon.PunBehaviour
{
    public GameObject Disparo;
   // public GameObject CuboGordo;
    public Animator animatorCharacter;
    private string DisparoName = "BolaDeEnergiaPlayerOnline";
    public bool Shot =false;

    public Transform ShotPosition;

    // Update is called once per frame
    public void  Start()
    {
        enabled = photonView.isMine;
        animatorCharacter = GetComponentInChildren<Animator>();
        /*
        if (!photonView.isMine)
        {
            Destroy(this);
        }*/
    }
    void Update()
    {
        if (Input.GetKeyUp(KeyCode.Z))
        {
            photonView.RPC("Disparar", PhotonTargets.All);

        }

        if (Shot == true)
        {
            /*
            if (photonView.isMine)
            {*/
            Debug.Log("EstaActivoElSHot"); ///////

                photonView.RPC("DispararMobile", PhotonTargets.All);
                //Instantiate(Disparo, this.transform.position, this.transform.rotation * Quaternion.Euler(0, -180, 0));
                animatorCharacter.ResetTrigger("Walk");
                animatorCharacter.ResetTrigger("Idle");
                //animatorCharacter.ResetTrigger("Idle");
                animatorCharacter.ResetTrigger("Jump");
                animatorCharacter.SetTrigger("Shot");

                //GameObject clone = (GameObject)Instantiate(Disparo, this.transform.localPosition, this.transform.localRotation * Quaternion.Euler(0, -180, 0) );


                //GameObject clone = (GameObject)Instantiate(Disparo, this.transform.position, this.transform.rotation * Quaternion.Euler(0, -180, 0));


                //GameObject clone = Instantiate((GameObject)Resources.Load("BolaDeEnergiaPlayerOnline"), this.transform.position, this.transform.rotation * Quaternion.Euler(0, -180, 0));


                //GameObject clone = Instantiate((GameObject)Resources.Load("BolaDeEnergiaPlayerOnline"), ShotPosition.position, this.transform.rotation * Quaternion.Euler(0, -180, 0));
                
            
            //GameObject clone = Instantiate((GameObject)Resources.Load("BolaDeEnergiaPlayerOnline"), ShotPosition.position, this.transform.rotation * Quaternion.Euler(0, -180, 0)); /////
            GameObject clone = PhotonNetwork.Instantiate("BolaDeEnergiaPlayerOnline", ShotPosition.position, this.transform.rotation * Quaternion.Euler(0, -180, 0), 0);

            // GameObject clone2 = Instantiate((GameObject)Resources.Load("Cube"), this.transform.position, this.transform.rotation * Quaternion.Euler(0, -180, 0));
            // GameObject clone = PhotonNetwork.Instantiate(Resources.Load("BolaDeEnergiaPlayerOnline"), this.transform.position, this.transform.rotation * Quaternion.Euler(0, -180, 0);


            Destroy(clone, 5);

                // Disparo.transform.rotation = Quaternion.Euler(0, -180, 1);

                // Debug.Log("Shot");
            //}
        }


        /*
            if (photonView.isMine)
        {


            // Disparar();

            photonView.RPC("Disparar", PhotonTargets.All);
        } */
    }
    [PunRPC]
    public void Disparar()
    {
        /*
        if (photonView.isMine)
        {
            /*
            if(Input.GetButtonDown("Shot"))
            { */

           // if (Input.GetKeyUp(KeyCode.Z))
           // {
                animatorCharacter.ResetTrigger("Walk");
                animatorCharacter.ResetTrigger("Idle");
                //animatorCharacter.ResetTrigger("Idle");
                animatorCharacter.ResetTrigger("Jump");
                animatorCharacter.SetTrigger("Shot");

        // GameObject clone = (GameObject)Instantiate(Disparo, this.transform.position, this.transform.rotation * Quaternion.Euler(0, -180, 0));


        //GameObject clone = Instantiate((GameObject) Resources.Load("BolaDeEnergiaPlayerOnline"), this.transform.position, this.transform.rotation * Quaternion.Euler(0, -180, 0));
                GameObject clone = Instantiate((GameObject) Resources.Load("BolaDeEnergiaPlayerOnline"), /*this.transform.position*/ ShotPosition.position, this.transform.rotation * Quaternion.Euler(0, -180, 0));
               // GameObject clone2 = Instantiate((GameObject)Resources.Load("Cube"), this.transform.position, this.transform.rotation * Quaternion.Euler(0, -180, 0));
                Destroy(clone, 5);
                // Disparo.transform.rotation = Quaternion.Euler(0, -180, 1);

                // Debug.Log("Shot");
           // }
       // }
    }

    public void pescarDisparo()
    {
        photonView.RPC("DispararMobile", PhotonTargets.All);
    }


    [PunRPC]
    public void DispararMobile()
    {
        if (photonView.isMine)
        {
            Shot = true;
            Invoke("DispararMobileOff", 0.05f);
        }

        // if (photonView.isMine)
        // {


            /*
            if (photonView.isMine)
            {
                //photonView.RPC("DispararMobile", PhotonTargets.All);
                //Instantiate(Disparo, this.transform.position, this.transform.rotation * Quaternion.Euler(0, -180, 0));
                animatorCharacter.ResetTrigger("Walk");
                animatorCharacter.ResetTrigger("Idle");
                //animatorCharacter.ResetTrigger("Idle");
                animatorCharacter.ResetTrigger("Jump");
                animatorCharacter.SetTrigger("Shot");

                //GameObject clone = (GameObject)Instantiate(Disparo, this.transform.localPosition, this.transform.localRotation * Quaternion.Euler(0, -180, 0) );


                //GameObject clone = (GameObject)Instantiate(Disparo, this.transform.position, this.transform.rotation * Quaternion.Euler(0, -180, 0));


                //GameObject clone = Instantiate((GameObject)Resources.Load("BolaDeEnergiaPlayerOnline"), this.transform.position, this.transform.rotation * Quaternion.Euler(0, -180, 0));


                GameObject clone = Instantiate((GameObject)Resources.Load("BolaDeEnergiaPlayerOnline"),  ShotPosition.position, this.transform.rotation * Quaternion.Euler(0, -180, 0));


                // GameObject clone2 = Instantiate((GameObject)Resources.Load("Cube"), this.transform.position, this.transform.rotation * Quaternion.Euler(0, -180, 0));
                // GameObject clone = PhotonNetwork.Instantiate(Resources.Load("BolaDeEnergiaPlayerOnline"), this.transform.position, this.transform.rotation * Quaternion.Euler(0, -180, 0);


                Destroy(clone, 5);

                // Disparo.transform.rotation = Quaternion.Euler(0, -180, 1);

                // Debug.Log("Shot");
            }
            */


    }


    public void DispararMobileOff()
    {
        if (photonView.isMine)
        {
            Shot = false;
        }
    }
   // }
}
