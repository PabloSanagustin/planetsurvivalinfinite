﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Pause : MonoBehaviour
{
    public Spawner spawner;
    public GameObject pausepannel, pausebutton;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyUp(KeyCode.Escape))
        {
            Pausar();
        }
        /*
        if (pausepannel.activeSelf || spawner.WinMenu.activeSelf == true ) ///////
        {
            Time.timeScale = 0;
        }
        else if (pausepannel.activeSelf || spawner.WinMenu.activeSelf == false ) ////
        {
            Time.timeScale = 1;
        } */
    }

    public void Pausar()
    {
        Time.timeScale = 0;
        pausebutton.SetActive(false);
        pausepannel.SetActive(true);

    }
    public void UnPause()
    {
        Time.timeScale = 1;
        pausebutton.SetActive(true);
        pausepannel.SetActive(false);
    }

    public void GoToMenu()
    {
        SceneManager.LoadScene("Menu");
        Time.timeScale = 1;
    
    }


}
