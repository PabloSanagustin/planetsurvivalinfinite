﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class PlayerLifeOnline : Photon.PunBehaviour
{
    public AudioSource Damage;
    public AudioSource MoreLife;
    public float life;
    private Slider lifebar;

    public static float DamageEnemies = 1;
    public static float DamageBullet = 5;
    private GameObject Player;

    public int MaxWinLife = 30;
    public int MinWinLife = 3;

    public LoadingScreenBarSystem Loading;
    AsyncOperation async;

    private Rigidbody rigidbodyPlayer;

    private Color myColor;
    private MeshRenderer meshRenderer;

    void Start()
    {
        /*
        if (!photonView.isMine)
        {
            Destroy(this);
        } */
        enabled = photonView.isMine;

        lifebar = GameObject.FindGameObjectWithTag("lifebar").GetComponent<Slider>();
        Player = GameObject.FindGameObjectWithTag("Player");
        life = 100.0f;


        rigidbodyPlayer = GetComponent<Rigidbody>();

        meshRenderer = GetComponent<MeshRenderer>();
        myColor = new Color(Random.value, Random.value, Random.value, 1.0f);
        myColor = meshRenderer.material.color;
    }

    // Update is called once per frame
    void Update()
    {
        if (photonView.isMine)
        {
            /*
            if (!photonView.isMine)
            {
                Destroy(this);
            } */


            lifebar.value = life;
            if (transform.position.y <= -0.4)
            {
                life = 0;
            }
            if (transform.position.y >= 21.77f)
            {
                //transform.Translate(Vector3.up * -10);
                rigidbodyPlayer.velocity = Vector3.zero;
                rigidbodyPlayer.angularVelocity = Vector3.zero;
                //rigidbodyPlayer.tAddForce(movemen)
                //rigidbodyPlayer.velocity = Vector3(0,5,0);
                rigidbodyPlayer.AddForce(new Vector3(0, -5, 0), ForceMode.Impulse);
            }


            if (life <= 0)
            {
                GameOver();
                gameObject.SetActive(false);
                //StartCoroutine(DisconectAndLoad()); //////
                PhotonNetwork.Destroy(this.photonView);
                //Player.SetActive(false);
            }
        }

    }
    private void OnTriggerEnter(Collider other)
    {
        if (photonView.isMine)
        {
            if (other.tag == "Player")
            {
                if (photonView.isMine)
                {
                    Damage.Play();
                    life -= 10.0f;

                }
            }
            if (other.tag == "Enemy")
            {
                Damage.Play();
                life -= DamageEnemies;
            }
            if (other.tag == "EnemyShot")
            {
                Damage.Play();
                life -= DamageBullet;
            }
            if (other.tag == "MoreLife")
            {
                MoreLife.Play();
                other.gameObject.SetActive(false);
                life += Random.Range(MinWinLife, MaxWinLife);
            }
        }
    }
    public void GameOver()
    {
        if (photonView.isMine)
        {
            PlayerPrefs.SetInt("MyScore", ScoreManager.score);
            if (ScoreManager.score > ScoreManager.record)
            {
                PlayerPrefs.SetInt("MyRecord", ScoreManager.score);
            }

            SceneManager.LoadScene("GameOver");
            Loading.loadingScreen(3);
            //async = SceneManager.LoadSceneAsync(3);

            Loading.gameObject.SetActive(true);
        }
    }

    IEnumerator DisconectAndLoad()
    {
        if (photonView.isMine)
        {
            PhotonNetwork.Disconnect();

            // while(PhotonNetwork.IsConnected)
            yield return null;
        }

    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
       // if (photonView.isMine)
       // {
            if (stream.isWriting)
            {
                stream.SendNext(life);
            }
            else
            {
                life = (int)stream.ReceiveNext();
            }
      //  }
    }
   public void DamagePlayerOnline()
    {
        if (photonView.isMine)
        {
            Debug.Log("DamageOtherPlayer");
            life -= Random.Range(10, 30);
        }
    }
}
