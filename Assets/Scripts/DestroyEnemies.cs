﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyEnemies : MonoBehaviour
{
    public GameObject Explosion;

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Enemy")
        {
            Destroy(other.gameObject);
            GameObject clone = (GameObject)Instantiate(Explosion, other.gameObject.transform.position, other.gameObject.transform.rotation);
            Destroy(clone, 3);
            ScoreManager.score += Random.Range(1, 5);
        }

        if(other.tag == "ExplosionEnemy")
        {
            Destroy(gameObject);
        }
    }
}
