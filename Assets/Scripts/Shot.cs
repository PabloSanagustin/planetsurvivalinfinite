﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shot : MonoBehaviour
{
    public GameObject Disparo;
    private Animator animatorCharacter;

    // Update is called once per frame
    public void Start()
    {
        animatorCharacter = GetComponentInChildren<Animator>();
    }
    void Update()
    {
        Disparar();
    }

    public void Disparar()
    {
        /*
        if(Input.GetButtonDown("Shot"))
        { */
        if(Input.GetKeyUp(KeyCode.Z))
        {
            animatorCharacter.ResetTrigger("Walk");
            animatorCharacter.ResetTrigger("Idle");
            //animatorCharacter.ResetTrigger("Idle");
            animatorCharacter.ResetTrigger("Jump");
            animatorCharacter.SetTrigger("Shot");

            GameObject clone = (GameObject)Instantiate(Disparo, this.transform.position, this.transform.rotation * Quaternion.Euler(0, -180,0) );
            Destroy(clone, 5);
            // Disparo.transform.rotation = Quaternion.Euler(0, -180, 1);

           // Debug.Log("Shot");
        }
    }

    public void DispararMobile()
    {
        //Instantiate(Disparo, this.transform.position, this.transform.rotation * Quaternion.Euler(0, -180, 0));
        animatorCharacter.ResetTrigger("Walk");
        animatorCharacter.ResetTrigger("Idle");
        //animatorCharacter.ResetTrigger("Idle");
        animatorCharacter.ResetTrigger("Jump");
        animatorCharacter.SetTrigger("Shot");

        GameObject clone = (GameObject)Instantiate(Disparo, this.transform.localPosition, this.transform.localRotation * Quaternion.Euler(0, -180, 0) );
        Destroy(clone, 5);

        // Disparo.transform.rotation = Quaternion.Euler(0, -180, 1);

       // Debug.Log("Shot");
    }
}
