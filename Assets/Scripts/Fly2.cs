﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fly2 : MonoBehaviour
{

    public Transform myTranform;
    public float fly = 0.01f;
    public float flyY = 0.01f;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Z))
        {
            myTranform.transform.Translate(new Vector3(0, flyY, 0) * fly * Time.deltaTime);

        }
    }
}
