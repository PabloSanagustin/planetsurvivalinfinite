﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Spawner : Pause
{
    public float TiempoParaQueAparezca = 5.0f;
    public float EsperarAlPrincipio = 10f;
    public GameObject[] SpawningEnemies;
    public GameObject WinMenu;
    public bool QuitWin = false;
    public Button button;
    public AudioSource audioSourceSpawner;

    bool buttonPressed;


    public Button buttonExitToMenu;

    public void start()
    {
        //audioSourceSpawner = GetComponent<AudioSource>();

        button.onClick.AddListener(ContinueTheGame);
        /*
        if (pausepannel.activeSelf || WinMenu.activeSelf == true) ///////
        {
            Time.timeScale = 0;
        }
        else if (pausepannel.activeSelf || WinMenu.activeSelf == false) ////
        {
            Time.timeScale = 1;
        }
        */
    }


    private void OnMouseDown (Button btn)
    {
        buttonPressed = true;
    }
    private void OnMouseUp(Button btn)
    {
        buttonPressed = false;
    }
    public void Update()
    {
        if (ScoreManager.score >= 0)
        {
            TiempoParaQueAparezca = 10f;
            PlayerLife.DamageEnemies = 1;
            PlayerLife.DamageBullet = 3f;
            //Debug.Log("Subida de nivel");
        }
        
        if (ScoreManager.score >= 10)
        {
            TiempoParaQueAparezca = 5f;
            PlayerLife.DamageEnemies = 2;
            PlayerLife.DamageBullet = 6;
           // Debug.Log("Subida de nivel2");
        }
        
        if (ScoreManager.score >= 80)
        {
            TiempoParaQueAparezca = 2.5f;
            PlayerLife.DamageEnemies = 4;
            PlayerLife.DamageBullet = 12;
           // Debug.Log("Subida de nivel3");
        }
        if (ScoreManager.score >= 300)
        {
            TiempoParaQueAparezca = 1.25f;
            PlayerLife.DamageEnemies = 8;
            PlayerLife.DamageBullet = 24;
            //Debug.Log("Subida de nivel3");
        }
        if (ScoreManager.score >= 600)
        {
            TiempoParaQueAparezca = 0.6f;
            PlayerLife.DamageEnemies = 16;
            PlayerLife.DamageBullet = 30;
           // Debug.Log("Subida de nivel4");
        }
        
        YouWin50Points();
        //ContinueTheGame();
        //button.onClick.AddListener(ContinueTheGame)

        
        if(pausepannel.activeSelf || WinMenu.activeSelf == true) ///////
        {
            Time.timeScale = 0;
        }
        else if(pausepannel.activeSelf || WinMenu.activeSelf == false) ////
        {
            Time.timeScale = 1;
        }
        if(buttonPressed)
         {
            Time.timeScale = 1;
         }
       

        
        

        /*
        if (WinMenu.activeSelf == true) ///////
        {
            Time.timeScale = 0;
        }
        else if (WinMenu.activeSelf == false) ////
        {
            Time.timeScale = 1;
        } */

    }

    public void TimeScale1()
    {
        Time.timeScale = 1;
    }

    public void YouWin50Points()
    {
        if (ScoreManager.score >= 50)
        {
            TiempoParaQueAparezca = 0.6f;
            //PlayerLife.DamageEnemies = 26;
            //PlayerLife.DamageBullet = 40;
           // Debug.Log("Subida de nivel5");
            QuitWin = true;
            if (QuitWin == true)
            {
                YouWin();
                WinMenu.SetActive(true);
                //ContinueTheGame();
            }
            //YouWin();

        }
        if(ScoreManager.score >= 60)
        {
            Time.timeScale = 1;
            WinMenu.SetActive(false);
        }

        /*
        if(ScoreManager.score >= 55)
        {
            Time.timeScale = 1;
            WinMenu.SetActive(false);
            QuitWin = false;
            //YouWin50Points();
            ScoreManager.score = 60;
        }
        */
    }

    public void YouWin()
    {
        if (WinMenu.activeInHierarchy == true )
        {
            Time.timeScale = 0;
            if(button)
            //pausebutton.SetActive(false);
            WinMenu.SetActive(true);

            if (pausepannel.activeSelf || WinMenu.activeSelf == true) ///////
            {
                Time.timeScale = 0;
            }
            else if (pausepannel.activeSelf || WinMenu.activeSelf == false) ////
            {
                Time.timeScale = 1;
            }

        }
        

    }

    public void ContinueTheGame()
    {

            Time.timeScale = 1;
            WinMenu.SetActive(false);
            QuitWin = false;
            //YouWin50Points();
            ScoreManager.score = 60;
        /*
        if (pausepannel.activeSelf || WinMenu.activeSelf == true) ///////
        {
            Time.timeScale = 0;
        }
        else if (pausepannel.activeSelf || WinMenu.activeSelf == false) ////
        {
            Time.timeScale = 1;
        }
        */

    }


    public void Start()
    {
        WinMenu.SetActive(false);
        InvokeRepeating("Spawn", EsperarAlPrincipio, TiempoParaQueAparezca);
    }
    public void Spawn()
    {
        audioSourceSpawner.Play();
        int SpawnPointX = Random.Range(1, 10);
        int SpawnPointY = Random.Range(1, 10);

        int SpawnPointZ = Random.Range(1, 10);

        Vector3 spawnPositon = transform.position + new Vector3(SpawnPointX, 0, SpawnPointZ);
        
        if (ScoreManager.score >= 0)
        {
            
            GameObject piClone = Instantiate(SpawningEnemies[Random.Range(0, SpawningEnemies.Length)], /*transform.position*/ spawnPositon, transform.rotation) as GameObject;
        }

       
        if (ScoreManager.score >= 10)
        {
            
            GameObject piClone = Instantiate(SpawningEnemies[Random.Range(0, SpawningEnemies.Length)],  spawnPositon, transform.rotation) as GameObject;
            GameObject piClone2 = Instantiate(SpawningEnemies[Random.Range(0, SpawningEnemies.Length)],  spawnPositon, transform.rotation) as GameObject;
        }
        
       if (ScoreManager.score >= 80)
       {
            
            GameObject piClone = Instantiate(SpawningEnemies[Random.Range(0, SpawningEnemies.Length)], spawnPositon, transform.rotation) as GameObject;
           GameObject piClone2 = Instantiate(SpawningEnemies[Random.Range(0, SpawningEnemies.Length)],  spawnPositon, transform.rotation) as GameObject;
           GameObject piClone3 = Instantiate(SpawningEnemies[Random.Range(0, SpawningEnemies.Length)],  spawnPositon, transform.rotation) as GameObject;
       }

       
       if (ScoreManager.score >= 150)
       {
            audioSourceSpawner.Play();
            GameObject piClone = Instantiate(SpawningEnemies[Random.Range(0, SpawningEnemies.Length)],  spawnPositon, transform.rotation) as GameObject;
           GameObject piClone2 = Instantiate(SpawningEnemies[Random.Range(0, SpawningEnemies.Length)],  spawnPositon, transform.rotation) as GameObject;
           GameObject piClone3 = Instantiate(SpawningEnemies[Random.Range(0, SpawningEnemies.Length)],  spawnPositon, transform.rotation) as GameObject;
           GameObject piClone4 = Instantiate(SpawningEnemies[Random.Range(0, SpawningEnemies.Length)],  spawnPositon, transform.rotation) as GameObject;
       }
       
       if (ScoreManager.score >= 300)
       {
           GameObject piClone = Instantiate(SpawningEnemies[Random.Range(0, SpawningEnemies.Length)],  spawnPositon, transform.rotation) as GameObject;
           GameObject piClone2 = Instantiate(SpawningEnemies[Random.Range(0, SpawningEnemies.Length)],  spawnPositon, transform.rotation) as GameObject;
           GameObject piClone3 = Instantiate(SpawningEnemies[Random.Range(0, SpawningEnemies.Length)],  spawnPositon, transform.rotation) as GameObject;
           GameObject piClone4 = Instantiate(SpawningEnemies[Random.Range(0, SpawningEnemies.Length)],  spawnPositon, transform.rotation) as GameObject;
           GameObject piClone5 = Instantiate(SpawningEnemies[Random.Range(0, SpawningEnemies.Length)],  spawnPositon, transform.rotation) as GameObject;
       }

       if (ScoreManager.score >= 500)
       {
           GameObject piClone = Instantiate(SpawningEnemies[Random.Range(0, SpawningEnemies.Length)],  spawnPositon, transform.rotation) as GameObject;
           GameObject piClone2 = Instantiate(SpawningEnemies[Random.Range(0, SpawningEnemies.Length)],  spawnPositon, transform.rotation) as GameObject;
           GameObject piClone3 = Instantiate(SpawningEnemies[Random.Range(0, SpawningEnemies.Length)],  spawnPositon, transform.rotation) as GameObject;
           GameObject piClone4 = Instantiate(SpawningEnemies[Random.Range(0, SpawningEnemies.Length)],  spawnPositon, transform.rotation) as GameObject;
           GameObject piClone6 = Instantiate(SpawningEnemies[Random.Range(0, SpawningEnemies.Length)],  spawnPositon, transform.rotation) as GameObject;

       }
       

    }


}
