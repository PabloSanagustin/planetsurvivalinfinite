﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Monetization;

public class Adcaller : MonoBehaviour
{
    // Start is called before the first frame update
    private string adid = "3708481";
    private string videoad = "video";

    private void Awake()
    {
        if (Application.platform == RuntimePlatform.IPhonePlayer)
            adid = "3708480";
        else if (Application.platform == RuntimePlatform.Android)
            adid = "3708481";
    }
    void Start()
    {



        Monetization.Initialize(adid, true);

        if (Monetization.IsReady(videoad))
        {
            ShowAdPlacementContent ad = null;
            ad = Monetization.GetPlacementContent(videoad) as ShowAdPlacementContent;
            if (ad != null)
            {
                ad.Show();
            }
        }
    }

    // Update is called once per frame
    public void Adshower()
    {
        if(Monetization.IsReady(videoad))
        {
            ShowAdPlacementContent ad = null;
            ad = Monetization.GetPlacementContent(videoad) as ShowAdPlacementContent;
            if(ad != null)
            {
                ad.Show();
            }
        }
    }
}
