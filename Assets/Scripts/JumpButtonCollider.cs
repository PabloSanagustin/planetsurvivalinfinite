﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpButtonCollider : MonoBehaviour
{
    //bool isSelected;
    public PlayerMovementJump2 playerMovementJump;

    public void OnMouseDown()
    {
        Debug.Log("Jump");
        playerMovementJump.jump();
    }
    public void OnMouseUp()
    {
        playerMovementJump.RotateOff();
    }
}
