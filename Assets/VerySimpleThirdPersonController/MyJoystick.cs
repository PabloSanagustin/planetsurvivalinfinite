using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class MyJoystick : MonoBehaviour, IDragHandler, IPointerUpHandler, IPointerDownHandler {
	private Image bgImage;
	private Image joystickImage;
	private Vector3 inputVector;
	
	bool started = false;

    public Animator animatorCharacter;
    private PlayerMovementJump2 PlayerMovement;


    public GameObject GenerateSeed;

    private void Start(){
		bgImage = GetComponent<Image> ();
		joystickImage = transform.GetChild (0).GetComponent<Image> ();

       // animatorCharacter = GameObject.FindGameObjectWithTag("Player").GetComponentInChildren<Animator>();
        PlayerMovement = GameObject.FindGameObjectWithTag("Player").GetComponentInChildren<PlayerMovementJump2>();

        //GenerateSeed = GameObject.Find("GenerateSeed")();
    }

    public void Update()
    {
        /*
        if (/*Input.GetAxis("Vertical") */ /*inputVector.y <= -0.01 */ /*<= -0.01)
        {
            GenerateSeed.transform.localPosition = new Vector3(GenerateSeed.transform.localPosition.x, GenerateSeed.transform.localPosition.y, GenerateSeed.transform.localPosition.z * -1);
        }

        if (/*Input.GetAxis("Vertical") */ /*inputVector.y > -0.01 */ /* > -0.01)
        {
            GenerateSeed.transform.localPosition = new Vector3(GenerateSeed.transform.localPosition.x, GenerateSeed.transform.localPosition.y, GenerateSeed.transform.localPosition.z * +1);
        } */
    }
    public virtual void OnPointerDown(PointerEventData ped){
		OnDrag (ped);
		if (!started) {
			started = true;
		}
        /*
        if (PlayerMovement.IsGrounded == true)
        {
            animatorCharacter.SetTrigger("Walk");
            animatorCharacter.ResetTrigger("Idle");
            //animatorCharacter.ResetTrigger("Idle");
            animatorCharacter.ResetTrigger("Jump");
        }
        */
        /*
        if (PlayerMovement.IsGrounded == false)
        {
            animatorCharacter.SetTrigger("Jump");
            animatorCharacter.ResetTrigger("Idle");
            //animatorCharacter.ResetTrigger("Idle");
            animatorCharacter.ResetTrigger("Walk");


        }
        */
        //animatorCharacter.SetTrigger("Walk");

    }
	public virtual void OnPointerUp(PointerEventData ped){
		inputVector = Vector3.zero;
		joystickImage.rectTransform.anchoredPosition = Vector3.zero;

        //animatorCharacter.SetTrigger("Idle");
        /*
        if (PlayerMovement.IsGrounded == false)
        {
            animatorCharacter.SetTrigger("Idle");
            animatorCharacter.ResetTrigger("Walk");
            //animatorCharacter.ResetTrigger("Idle");
            animatorCharacter.ResetTrigger("Jump");
        }
        */
    }
	public virtual void OnDrag(PointerEventData ped){
        //animatorCharacter.SetTrigger("Walk");

        /*
        if (PlayerMovement.IsGrounded == true)
        {
            animatorCharacter.SetTrigger("Walk");
            animatorCharacter.ResetTrigger("Idle");
            //animatorCharacter.ResetTrigger("Idle");
            animatorCharacter.ResetTrigger("Jump");
        }
        */
        /*
        if (PlayerMovement.IsGrounded == false)
        {
            animatorCharacter.SetTrigger("Jump");
            animatorCharacter.ResetTrigger("Idle");
            //animatorCharacter.ResetTrigger("Idle");
            animatorCharacter.ResetTrigger("Walk");


        }
        */

        Vector2 pos;
		if (RectTransformUtility.ScreenPointToLocalPointInRectangle (bgImage.rectTransform, ped.position, ped.pressEventCamera, out pos)) {
			pos.x = (pos.x / bgImage.rectTransform.sizeDelta.x);
			pos.y = (pos.y / bgImage.rectTransform.sizeDelta.y);
			inputVector = new Vector3 (pos.x * 2 + 1, 0, pos.y * 2 - 1);
			inputVector = (inputVector.magnitude > 1.0f) ? inputVector.normalized : inputVector;
			joystickImage.rectTransform.anchoredPosition = new Vector3(inputVector.x * (bgImage.rectTransform.sizeDelta.x / 2), inputVector.z * (bgImage.rectTransform.sizeDelta.y / 2));
		}
	}
	public float Horizontal(){
		if (inputVector.x != 0)
			return inputVector.x;
		else
			return Input.GetAxis ("Horizontal");
	}
	public float Vertical(){
		if (inputVector.z != 0)
			return inputVector.z;
		else
			return Input.GetAxis ("Vertical");
	}

    public void CaminarOn()
    {
        if (PlayerMovement.IsGrounded == true)
        {
            animatorCharacter.SetTrigger("Walk");
            animatorCharacter.ResetTrigger("Idle");
            //animatorCharacter.ResetTrigger("Idle");
            animatorCharacter.ResetTrigger("Jump");
        }
    }

    public void caminarOff()
    {
        animatorCharacter.SetTrigger("Idle");
    }
}